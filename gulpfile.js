'use strict';

const gulp = require('gulp'),
    Zpydify = require('zpydify');

const clientBuild = new Zpydify('./client', 'system', 'bgGreen');
const serverBuild = new Zpydify('./server', 'commonjs', 'bgBlue');

gulp.task('default', ['hack']);
gulp.task('hack', ['launch']);
gulp.task('launch', ['watch-server', 'watch-client'], () => serverBuild.launch());


/****************************************************************
 * CLIENT TASKS
 ****************************************************************/

/* WATCHERS **/
gulp.task('watch-client', [
    'watch-client-app-ts', 'watch-client-test-ts', 'watch-client-views', 'watch-client-styles']);
gulp.task('watch-client-app-ts', ['test-client'], () => gulp.watch(
    clientBuild.src.appTs, ['test-client']));
gulp.task('watch-client-test-ts', [], () => gulp.watch(
    clientBuild.src.testTs, ['test-client']));
gulp.task('watch-client-views', ['compile-client-views'], () => gulp.watch(
    clientBuild.src.views, ['compile-client-views']));
gulp.task('watch-client-styles', ['compile-client-styles'], () => gulp.watch(
    clientBuild.src.styles, ['compile-client-styles']));

/* LINTERS */
gulp.task('lint-client-app-ts', () => clientBuild.lintTs(clientBuild.src.appTs));
gulp.task('lint-client-test-ts', () => clientBuild.lintTs(clientBuild.src.testTs));

/* CLEANERS */
gulp.task('clean-client-app-js', () => clientBuild.clean(clientBuild.dest.appJs));
gulp.task('clean-client-test-js', () => clientBuild.clean(clientBuild.dest.testJs));
gulp.task('clean-client-views', () => clientBuild.clean(clientBuild.dest.views));
gulp.task('clean-client-styles', () => clientBuild.clean(clientBuild.dest.styles));

/* COMPILERS */
gulp.task('compile-client-app-ts', ['lint-client-app-ts', 'clean-client-app-js'], () => clientBuild.compileTs(
    clientBuild.src.appTs, clientBuild.dest.appJs));
gulp.task('compile-client-test-ts', ['lint-client-test-ts', 'clean-client-test-js'], () => clientBuild.compileTs(
    clientBuild.src.testTs, clientBuild.dest.testJs));
gulp.task('compile-client-views', ['clean-client-views'], () => clientBuild.compileJade(
    clientBuild.src.views, clientBuild.dest.views));
gulp.task('compile-client-styles', ['clean-client-styles'], () => clientBuild.move(
    clientBuild.src.styles, clientBuild.dest.styles));
    
gulp.task('test-client', ['compile-client-app-ts', 'compile-client-test-ts'], () =>
    clientBuild.karma(clientBuild.rootPath));


/****************************************************************
 * SERVER TASKS
 ***************************************************************/

/* WATCHERS **/
gulp.task('watch-server', ['watch-server-app', 'watch-server-test']);
gulp.task('watch-server-app', ['test-server'], () => gulp.watch(
    serverBuild.src.appTs, ['test-server']));
gulp.task('watch-server-test', [], () => gulp.watch(
    serverBuild.src.testTs, ['test-server']));

/* LINTERS */
gulp.task('lint-server-app-ts', () => serverBuild.lintTs(serverBuild.src.appTs));
gulp.task('lint-server-test-ts', () => serverBuild.lintTs(serverBuild.src.testTs));

/* CLEANERS */
gulp.task('clean-server-app-js', () => serverBuild.clean(serverBuild.dest.appJs));
gulp.task('clean-server-test-js', () => serverBuild.clean(serverBuild.dest.testJs));

/* COMPILERS */
gulp.task('compile-server-app-ts', ['clean-server-app-js', 'lint-server-app-ts'], () => serverBuild.compileTs(
    serverBuild.src.appTs, serverBuild.dest.appJs));
gulp.task('compile-server-test-ts', ['clean-server-test-js', 'lint-server-test-ts'], () => serverBuild.compileTs(
    serverBuild.src.testTs, serverBuild.dest.testJs));

gulp.task('test-server', ['compile-server-app-ts', 'compile-server-test-ts'], () =>
    serverBuild.mocha(serverBuild.dest.testJs));