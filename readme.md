#Overview
`zpyd-ts-angular2express` is a boilerplate project for angular2
projects served using express, with automated linting and compilation support
for typescript on both server and client using [zpydify](http://bitbucket.org/zpydee/zpydify),
one of the project's key dev dependencies.
#Usage
1. Clone project from bitbucket
2. cd into directory and install npm dependencies
3. run gulp
```
git clone git:bitbucket.org:zpydee/zpyd-ts-angular2express
cd zpyd-ts-angular2express
npm install
gulp
```