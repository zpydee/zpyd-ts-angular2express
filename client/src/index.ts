/// <reference path="../../typings/main.d.ts" />
/// <reference path="./interfaces.ts" />

import {bootstrap}    from 'angular2/platform/browser';
import {AppComponent} from './components/app.component';

bootstrap(AppComponent);
