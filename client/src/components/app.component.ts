import {Component} from 'angular2/core';

@Component({
    selector: 'my-app',
    templateUrl: '/components/app.component.html'
})
export class AppComponent { }
