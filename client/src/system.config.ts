System.config({
    map: {
        'systemjs': 'node_modules/systemjs/dist/system.js'
    },
    packages: {
        app: {
            format: 'register',
            defaultExtension: 'js'
        }
    }
})