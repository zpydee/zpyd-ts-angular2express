interface IConfigurator {
    rootPath: string;
    mode: string;
    port: number;
    buildFolder: string;
}
