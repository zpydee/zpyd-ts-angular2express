import * as path from 'path';

class Configurator implements IConfigurator {
    public rootPath: string = path.normalize(__dirname + '/../../../..');
    public port: number = process.env.PORT || 3283;
    public mode: string;
    public buildFolder: string;

    constructor(mode: string) {
        this.mode = mode;
        this.buildFolder = mode === 'dev' ? 'compiled' : 'dist';
    }
}

export {Configurator}
