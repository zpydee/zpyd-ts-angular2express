import * as express from 'express';
export function init(app: express.Express, config: IConfigurator): void {
    'use strict';

    app.use(express.static(`${config.rootPath}/client/${config.buildFolder}/styles/`));
    app.use(express.static(`${config.rootPath}/client/${config.buildFolder}/views/`));
    app.use(express.static(`${config.rootPath}/client/${config.buildFolder}/`));
    app.use(express.static(`${config.rootPath}/`));
}
