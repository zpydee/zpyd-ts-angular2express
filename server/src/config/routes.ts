import * as express from 'express';

export function init(app: express.Express): void {
    'use strict';

    app.get('/', (req: express.Request, res: express.Response) => {
        res.render(`index.html`);
    });
}

