/// <reference path="../../typings/main.d.ts" />
/// <reference path="./interfaces.ts" />

import * as express from 'express';
import {Configurator} from './config/Configurator';
import * as middleware from './config/middleware';
import * as routes from './config/routes';

const config: IConfigurator = new Configurator(process.env.NODE_ENV);
const app: express.Express = express();

middleware.init(app, config);
routes.init(app);

app.listen(config.port, (err: express.Errback) => {
    if (err) {
        console.error(err);
    }
    console.log(`-----------------------------------------------------\n*** SUCCESFULLY ZPYDIFIED ***\nlistening on port ${config.port} in ${config.mode} mode\nserving app from root ${config.rootPath}\n-----------------------------------------------------`);
});
